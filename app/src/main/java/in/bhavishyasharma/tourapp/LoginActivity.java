package in.bhavishyasharma.tourapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private Activity thisActivity;
    private Button loginButton;
    private TextView signupButton;
    private EditText emailInput;
    private EditText passwordInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        thisActivity=this;
        SharedPreferences sharedPref = thisActivity.getSharedPreferences(getString(R.string.user_preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(getString(R.string.user_details));
        editor.remove(getString(R.string.user_token));
        editor.apply();
        this.loginButton=(Button)findViewById(R.id.login_button);
        this.signupButton=(TextView)findViewById(R.id.signup_button);
        this.emailInput=(EditText)findViewById(R.id.email_input);
        this.passwordInput=(EditText)findViewById(R.id.password_input);
        this.loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginTask loginTask=new LoginTask();
                loginTask.execute(emailInput.getText().toString(),passwordInput.getText().toString());
            }
        });
        this.signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private class LoginTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient();
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode objectNode=mapper.createObjectNode();
            objectNode.put("email",params[0]);
            objectNode.put("password",params[1]);
            RequestBody body = RequestBody.create(JSON, objectNode.toString());
            Request request = new Request.Builder()
                    .url(getString(R.string.server_url)+"/auth/token")
                    .post(body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                if (response.isSuccessful()) {
                    return response.body().string();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return mapper.createObjectNode().put("error","Login Failed!").toString();
        }

        @Override
        protected void onPostExecute(String result) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            JsonFactory factory = mapper.getFactory(); // since 2.1 use mapper.getFactory() instead
            JsonParser jp = null;
            try {
                jp = factory.createJsonParser(result);
                System.out.println(result);
                JsonNode userObj = mapper.readTree(jp);
                if(userObj.has("jwt") && userObj.has("user")){
                    ObjectMapper objectMapper = new ObjectMapper();
                    User user = objectMapper.treeToValue(userObj.get("user"),User.class);
                    SharedPreferences sharedPref = thisActivity.getSharedPreferences(getString(R.string.user_preference_file_key), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(getString(R.string.user_details), userObj.get("user").toString());
                    editor.putString(getString(R.string.user_token), userObj.get("jwt").toString());
                    editor.apply();
                    Snackbar.make(thisActivity.getCurrentFocus(), "Login Successfull.", Snackbar.LENGTH_LONG).show();
                    Intent i = new Intent(thisActivity, MainActivity.class);
                    thisActivity.startActivity(i);
                    thisActivity.finish();
                }
                else{
                    Snackbar.make(thisActivity.getCurrentFocus(), "Login Failed.", Snackbar.LENGTH_LONG).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
