package in.bhavishyasharma.tourapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.android.gms.maps.MapFragment;

public class MainActivity extends AppCompatActivity implements DrawerFragment.DrawerFragmentListener  {

    private Toolbar toolbar;
    private DrawerFragment drawerFragment;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.toolbar=(Toolbar)this.findViewById(R.id.toolbar);
        this.setSupportActionBar(this.toolbar);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (DrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        displayView(0);
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.user_preference_file_key), Context.MODE_PRIVATE);
        token=sharedPref.getString(getString(R.string.user_token),null);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position){
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.user_preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        switch (position) {
            case 0:
                fragment = new MapsFragment();
                break;
            case 1:
                fragment = new MonumentFragment();
                break;
            case 2:
                fragment = new HotelsFragment();
                break;
            case 3:
                if(token==null){
                    Intent i=new Intent(this,LoginActivity.class);
                    startActivity(i);
                    this.finish();
                }
                else {
                    editor.remove(getString(R.string.user_details));
                    editor.remove(getString(R.string.user_token));
                    editor.apply();
                    Intent i=new Intent(this,LoginActivity.class);
                    startActivity(i);
                    this.finish();
                }
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }
}
