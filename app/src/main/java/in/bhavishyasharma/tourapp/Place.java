package in.bhavishyasharma.tourapp;

import java.util.List;

/**
 * Created by bhavishyasharma on 05/03/17.
 */

public class Place {
    private String name;
    private String icon;
    private List<String> types;
    private String vicinity;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }
}
