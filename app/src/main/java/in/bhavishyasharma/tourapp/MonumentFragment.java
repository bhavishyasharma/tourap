package in.bhavishyasharma.tourapp;


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MonumentFragment extends Fragment {

    private Activity activity;
    private Fragment fragment;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<Place> placeList;
    private MonumentListAdapter adapter;

    public MonumentFragment() {
        // Required empty public constructor
        fragment=this;
        placeList=new ArrayList<>();
        if(getActivity()==null){
            System.out.println("Null Activity");
        }
        if(getContext()==null){
            System.out.println("Null Context");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        adapter=new MonumentListAdapter(activity,placeList);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_monument, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshPlaces();
            }
        });
        if(placeList.size()==0){
            fetchPlaces();
        }
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        return v;
    }

    public void refreshPlaces(){
        while(placeList.size()>0){
            placeList.remove(0);
        }
        fetchPlaces();
    }

    public void fetchPlaces(){
        PlaceListTask task=new PlaceListTask();
        if (ActivityCompat.checkSelfPermission(fragment.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(fragment.getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(fragment.getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
            return;
        }
        Criteria criteria=new Criteria();
        LocationManager locationManager = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        swipeRefreshLayout.setRefreshing(true);
        task.execute(location.getLatitude()+","+location.getLongitude(),"10000","amusement_park|art_gallery|mosque|museum|church|place_of_worship|hindu_temple");
    }

    private class PlaceListTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient();
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode objectNode=mapper.createObjectNode();
            HttpUrl.Builder urlBuilder = HttpUrl.parse("https://maps.googleapis.com/maps/api/place/nearbysearch/json").newBuilder();
            urlBuilder.addQueryParameter("location", params[0]);
            urlBuilder.addQueryParameter("radius", params[1]);
            urlBuilder.addQueryParameter("types", params[2]);
            urlBuilder.addQueryParameter("sensor", "true");
            urlBuilder.addQueryParameter("key", "AIzaSyATuUiZUkEc_UgHuqsBJa1oqaODI-3mLs0");
            String url = urlBuilder.build().toString();
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                if (response.isSuccessful()) {
                    return response.body().string();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return mapper.createObjectNode().put("error","Request Failed!").toString();
        }

        @Override
        protected void onPostExecute(String result) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            JsonFactory factory = mapper.getFactory(); // since 2.1 use mapper.getFactory() instead
            JsonParser jp = null;
            try{
                jp = factory.createJsonParser(result);
                JsonNode obj=mapper.readTree(jp);
                if(obj.isObject() && obj.has("results") && obj.get("results").isArray()){
                    ArrayNode results= (ArrayNode)obj.get("results");
                    for(JsonNode item: results){
                        Place place=mapper.treeToValue(item,Place.class);
                        placeList.add(place);
                    }
                    adapter.notifyDataSetChanged();
                }
                else{
                    Snackbar.make(fragment.getActivity().getCurrentFocus(), "Cannot load places.", Snackbar.LENGTH_LONG).show();
                }
            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    }

}
