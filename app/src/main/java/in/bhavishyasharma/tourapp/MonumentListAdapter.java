package in.bhavishyasharma.tourapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by bhavishyasharma on 05/03/17.
 */

public class MonumentListAdapter extends RecyclerView.Adapter<MonumentListAdapter.MonumentViewHolder> {

    private Context context;
    private List<Place> placesList;

    public MonumentListAdapter(Context context,List<Place> placesList) {
        this.context=context;
        this.placesList = placesList;
    }

    @Override
    public MonumentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.monument_list_item, parent, false);
        return new MonumentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MonumentViewHolder holder, int position) {
        final Place place = placesList.get(position);
        holder.name.setText(place.getName());
        holder.vicinity.setText(place.getVicinity());
        Picasso.with(context)
                .load(place.getIcon())
                .fit()
                .placeholder(R.drawable.ic_profile)
                .into(holder.icon);
    }

    @Override
    public int getItemCount() {
        return placesList.size();
    }

    public class MonumentViewHolder extends RecyclerView.ViewHolder {
        public ImageView icon;
        public TextView name;
        public RecyclerView typesRV;
        public TextView vicinity;

        public MonumentViewHolder(View view) {
            super(view);
            this.name =  (TextView)view.findViewById(R.id.nameTV);
            this.icon = (ImageView) view.findViewById(R.id.iconIV);
            this.vicinity = (TextView) view.findViewById(R.id.vicinityTV);
            this.typesRV = (RecyclerView)view.findViewById(R.id.typesRV);
        }
    }
}
