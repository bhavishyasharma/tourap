package in.bhavishyasharma.tourapp;

import java.util.Date;
import java.util.List;

/**
 * Created by bhavishyasharma on 05/03/17.
 */

public class User {
    private String _id;
    private String username;
    private String email;
    private String image;
    private boolean deleted;
    private Date deletedAt;
    private Date _createdAt;
    private Date _updatedAt;
    private Date lastLogin;
    private User _user;
    private String password;

    public Date get_createdAt() {
        return _createdAt;
    }

    public void set_createdAt(Date _createdAt) {
        this._createdAt = _createdAt;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Date get_updatedAt() {
        return _updatedAt;
    }

    public void set_updatedAt(Date _updatedAt) {
        this._updatedAt = _updatedAt;
    }

    public User get_user() {
        return _user;
    }

    public void set_user(User _user) {
        this._user = _user;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
